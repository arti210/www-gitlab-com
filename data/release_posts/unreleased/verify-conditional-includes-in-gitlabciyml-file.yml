---
features:
  primary:
  - name: "Add GitLab CI/CD configuration conditionally with `include`"
    available_in: [core, premium, ultimate]
    documentation_link: 'https://docs.gitlab.com/ee/#amazing'
    image_url: '/images/unreleased/conditional_include.png'
    reporter: dhershkovitch
    stage: verify
    categories:
    - Pipeline Authoring
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/276515'
    description: |
      [`include`](https://docs.gitlab.com/ee/ci/yaml/index.html#include) is one of the most popular keywords to use when writing a full CI/CD pipeline. If you are building larger pipelines, you are probably using the `include` keyword to bring external YAML configuration into your pipeline.

      In this release, we are expanding the power of the keyword so you can use `include` with [`rules](https://docs.gitlab.com/ee/ci/yaml/index.html#include) conditions. Now, you can decide when external CI/CD configuration should or shouldn't be included. This will help you write a standardized pipeline with the ability to dynamically modify itself based on the conditions you choose.
