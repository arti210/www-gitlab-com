speakers_block:
  speakers:
    - name: Sid Sijbrandij
      title: Co-founder, Chief Executive Officer, and GitLab Inc. Board of Directors Chair 
      role: Co-founder, <a href="/job-families/chief-executive-officer/">Chief Executive Officer (CEO)</a>, <a href="/job-families/board-of-directors/board_member/">Board of Directors</a>, Board Chair      
      gitlab: sytses
      country: USA
      locality: San Francisco, CA
      headshot: /images/headshots/Headshot-Sid.png
      story: >-
        Sid Sijbrandij (pronounced see-brandy) is the Co-founder, Chief Executive Officer and Board Chair of GitLab Inc., the DevOps platform. GitLab’s single application helps organizations deliver software faster and more efficiently while strengthening their security and compliance.
        

        Sid’s career path has been anything but traditional. He spent four years building recreational submarines for U-Boat Worx and while at Ministerie van Justitie en Veiligheid he worked on the Legis project, which developed several innovative web applications to aid lawmaking. He first saw Ruby code in 2007 and loved it so much that he taught himself how to program. In 2012, as a Ruby programmer, he encountered GitLab and discovered his passion for open source. Soon after, Sid commercialized GitLab, and by 2015 he led the company through Y Combinator’s Winter 2015 batch. Under his leadership, the company has grown with an estimated 30 million+ registered users from startups to global enterprises.

        
        Sid studied at the University of Twente in the Netherlands where he received an M.S. in Management Science. Sid was named one of the greatest minds of the pandemic by [Forbes](https://www.forbes.com/sites/elisabethbrier/2021/03/05/top-business-minds-of-the-pandemic/?sh=773356ef22ad) for spreading the gospel of remote work.
    - name: Robin Schulman
      title: Chief Legal Officer and Corporate Secretary 
      role: <a href="/job-families/legal/chief-legal-officer/">Chief Legal Officer and Corporate Secretary</a>
      gitlab: rschulman
      country: USA
      locality: San Francisco, CA      
      headshot: /images/headshots/Headshot-RobinSchulman.png
      story: >-
        Robin Schulman is the Chief Legal Officer and Corporate Secretary of GitLab Inc., the DevOps platform. GitLab’s single application helps organizations deliver software faster and more efficiently while strengthening their security and compliance.
        

        GitLab’s CLO and Corporate Secretary since 2019, Robin leverages her leadership experience scaling high growth technology companies to create a culture of compliance and set and manage the company’s overall global legal, compliance, policy and privacy philosophy and strategy. Supported by a talented team of attorneys and legal professionals, Robin ensures GitLab maintains balance across the company’s business activities and practices with its values and culture.  Additionally, Robin provides counsel to the GitLab Board of Directors across the entire spectrum of legal, compliance and corporate governance matters that pertain to the company. Robin is the executive sponsor of GitLab's women’s and pride team member resource groups and the unofficial sponsor of our #dog Slack channel.
        

        Prior to joining GitLab, Robin oversaw global legal affairs, public policy and compliance at Couchbase, Inc. (NASDAQ: BASE) as their SVP, Chief Legal Officer and Corporate Secretary. She also established, scaled, and led New Relic Inc. 's (NYSE: NEWR) global legal and compliance organization as their General Counsel and Chief Compliance Officer from pre-IPO to profitable public company. Prior to that, Robin was Legal Counsel to Adobe Inc. (NASDAQ: ADBE) where she led the legal function for several of Adobe’s Marketing and Creative Cloud products and specialized in advising high growth companies while an associate at Fenwick & West LLP, a law firm providing legal services to technology and life science companies.
        

        Robin earned a B.F.A. in Dramatic Writing and Film from New York University and a J.D. from Rutgers University School of Law - Newark. In 2017, Robin was honored  by the Silicon Valley Business Journal and the San Francisco Business Times with an award for Best Bay Area Corporate Counsel for a Public Company General Counsel. She has taught intellectual property law at Santa Clara Law School and is a board observer for a private biotech company. 

    - name: Brian Robins
      title: Chief Financial Officer
      role: <a href="/job-families/finance/chief-financial-officer/">Chief Financial Officer(CFO)</a>      
      country: United States
      locality: Washington, D.C.
      gitlab: brobins
      headshot: /images/headshots/Headshot-BrianRobins.png
      story: >-
              Brian Robins is the Chief Financial Officer at GitLab Inc., the DevOps platform. GitLab’s single application helps organizations deliver software faster and more efficiently while strengthening their security and compliance.
              

              As CFO, Brian is responsible for GitLab’s financial, data and business systems functions, including accounting, tax, treasury, corporate finance, IT, data science and investor relations. He helps to ensure GitLab’s finance and accounting systems and processes scale and grow with the company. His team oversees GitLab’s financial reporting, provides data driven decision support and offers strategic guidance to the business.
              

              Prior to GitLab, Brian served as CFO at Sisense, Cylance, AlienVault, and Verisign (NASDAQ: VRSN). As a 20-plus year veteran leading both private and public high-growth software companies, and with extensive experience with IPOs and M&As, Brian has a long, documented track record of improving financial performance, increasing productivity, and creating shareholder value. He lends this wisdom as a special advisor at Brighton Park Capital, L.P. and on the Advisory Council at ForgePoint Capital Cybersecurity.
              

              Brian holds a B.S. in Finance from Lipscomb University and an M.B.A from Vanderbilt University’s Owen Graduate School of Management.

    - name: Michael McBride
      title: Chief Revenue Officer
      role: <a href="/job-families/sales/chief-revenue-officer/">Chief Revenue Officer (CRO)</a>
      gitlab: mmcb
      country: USA
      locality: San Francisco, CA
      headshot: /images/headshots/HeadshotMichaelMcBride.png
      story: >-
            Michael McBride is the Chief Revenue Officer at GitLab Inc., the DevOps platform. GitLab’s single application helps organizations deliver software faster and more efficiently while strengthening their security and compliance.
            

            Since 2018, Michael has served as GitLab’s CRO. Michael applies his leadership experience from roles including sales, product, operations, and marketing as he and his organization partner with industry leading companies on their DevOps transformations so they can become more competitive, secure, and compliant.  He is responsible for worldwide sales, customer success, channels, field operations, professional services, alliances and business development teams that serve GitLab’s millions of users at organizations ranging from startups to the largest global enterprises. Michael is also executive co-sponsor of the women’s team member resource group and created the Women at GitLab leadership mentor program.
            

            Prior to GitLab, Michael served as Senior Vice President, Worldwide Field Operations at category-defining Lookout, a leader in mobile security. In that role, he helped transform the company from a consumer to an enterprise business.
            

            In roles prior to Lookout, he served as Vice President, Worldwide Sales at Meraki, the high growth networking company acquired by Cisco, as Vice President, Platform at Japanese gaming giant DeNA and as a member of the founding executive team at Lionside, which was acquired by DeNA. He also serves as board chair of a non-profit youth sports organization, an advisor and investor to high-growth technology companies, and passes on what he’s learned as a guest lecturer at Stanford.
            

            Michael graduated with an M.B.A. from Stanford Graduate School of Business and a B.S. in mechanical engineering from Stanford University. 

    - name: Wendy Barnes
      title: Chief People Officer
      role: <a href="/job-families/people-ops/chief-people-officer">Chief People Officer</a>
      gitlab: wendybarnes
      country: USA
      locality: San Francisco, CA
      headshot: /images/headshots/Headshot-WendyBarnes.png
      story: >-
            Wendy Barnes is the Chief People Officer at GitLab Inc., the DevOps platform. GitLab’s single application helps organizations deliver software faster and more efficiently while strengthening their security and compliance.
            

            As CPO, Wendy works closely with remote leaders to ensure the company’s transparent and [values](https://about.gitlab.com/handbook/values/#credit)-driven culture scales as the company grows. She is responsible for the overall team member experience including talent brand and acquisition, operations, people technology and analytics, diversity, inclusion and belonging and total rewards.
            

            Prior to joining GitLab, Wendy has led pre-IPO and Fortune 500 companies in HR leadership roles. She served as Chief Human Resources Officer at Palo Alto Networks (NASDAQ: PANW) where she was responsible for overseeing global human resources and talent, helping the company scale from 750 to over 5,000 employees. Previously, Wendy served as Vice President, Human and Workplace Resources at eHealth (NASDAQ: EHTH), where she led a global HR organization. She has also held HR leadership roles at Netflix (NASDAQ:  NFLX) and E*TRADE (NASDAQ: ETFC, recently acquired by Morgan Stanley).
            

            Wendy earned a B.S. in Business Management from Santa Clara University and currently serves as a member of the University's Leavey School of Business Advisory Board, where she provides insight and advice to the dean, faculty, and administration on workforce trends, how to develop programs within the university that enhance business education, and how to improve industry workforce processes. In addition to her role on the board, Wendy is also a facilitator, faculty advisor, and mentor on the Women’s Corporate Board Readiness Program, with a particular focus on women and underrepresented groups.

    - name: Eric Johnson
      title: Chief Technology Officer
      role: <a href="/job-families/engineering/engineering-management/#chief-technology-officer">Chief Technology Officer</a>
      gitlab: edjdev
      country: USA
      locality: San Francisco, CA
      headshot: /images/headshots/Headshot-EricJohnson.png
      story: >-
            Eric Johnson is the Chief Technology Officer at GitLab Inc., the DevOps platform. GitLab’s single application helps organizations deliver software faster and more efficiently while strengthening their security and compliance. 

            
            Eric has worked for 4 previous startups in the marketing technology, localization software, online video platform, and commercial drone industries. He’s helped them scale to various levels, including acquisitions and an IPO.

           
            Eric started at GitLab in September of 2017 and is responsible for a vast, highly talented team of software engineers, designers, and support personnel responsible for GitLab’s open source DevOps platform. An estimated 30 million+ registered users from startups to global enterprises use this platform. 

            
            Eric received his B.A in Philosophy from Villanova University. He currently serves as a board member of The Linux Foundation, a non-profit organization enabling mass innovation through open source. He also advises several startups.

    - name: Scott Williamson
      title: Chief Product Officer
      role: <a href="/job-families/product/chief-product-officer/">Chief Product Officer</a>
      gitlab: sfwgitlab
      country: USA
      locality: Boulder, CO
      headshot: /images/headshots/Headshot-ScottWilliamson.png
      story: >-
        Scott Williamson is the Chief Product Officer at GitLab Inc., the DevOps platform. GitLab’s single application helps organizations deliver software faster and more efficiently while strengthening their security and compliance.
              

        As Chief Product Officer, Scott is focused on delivering a stellar product experience to GitLab’s estimated 30 million+ registered users from startups to global enterprises. Scott’s responsibilities include product management, product operations, pricing, growth, and corporate development.
        

        Scott’s product leadership has helped scale three companies through periods of rapid growth, including GitLab Inc., SendGrid (acquired by Twilio, Inc.), and Wily Technology (acquired by CA Technologies). Scott’s greatest career satisfaction comes from building a world class product organization that enables team members to do the best work of their careers. Scott also shares his professional experience externally as a mentor for the Product Management Leadership fellowship at Workit, a professional development platform.
        

        Scott has a B.S. in Business Administration from the University of Kansas and an M.B.A. in Management of Technology from the University of California, Berkeley Haas School of Business.

    - name: Craig Mestel
      title: Interim Chief Marketing Officer, Vice President of Finance
      role: <a href="/job-families/legal/chief-marketing-officer/">Chief Marketing Officer</a>
      gitlab: cmestel
      country: USA
      locality: Pleasanton, CA
      headshot: /images/headshots/Headshot-CraigMestel.png
      story: >-
              Craig Mestel is the Interim Chief Marketing Officer and Vice President of Finance at GitLab Inc., the DevOps platform. GitLab’s single application helps organizations deliver software faster and more efficiently while strengthening their security and compliance. 
 
              

              Craig’s love of high-growth environments and passion for strong, data-driven decision-making is a valuable element of GitLab’s marketing organization. His ability to bring order out of chaos in order to solve problems has led to a proven track record of success in operations, analytics, and corporate finance. He has extensive experience scaling teams in SaaS, internet marketplaces, and AdTech businesses including Upwork, Inc. and Google.
 
              

              Much like his dual roles as Interim CMO and Vice President of Finance at GitLab, Craig double-majored in economics and computer systems engineering at Stanford University and received his M.B.A. from Harvard Business School.
