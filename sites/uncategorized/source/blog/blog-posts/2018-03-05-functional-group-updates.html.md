---
title: "GitLab's Functional Group Updates - March 5th - 9th"
author: Trevor Knudsen
author_gitlab: tknudsen
categories: company
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working from March 5th - 9th"
canonical_path: "/blog/2018/03/05/functional-group-updates/"
tags: inside GitLab, functional group updates
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Friday, right before our GitLab Team call, a different Functional Group gives an update to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### UX Team Team

[Presentation slides](https://docs.google.com/presentation/d/1rTrBJL26uN9QSLEDd7aFBiWxrohwM15pPsi3K7MsMmw/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/rXgjM602sSg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### CI Team

[Presentation slides](https://docs.google.com/presentation/d/14g6KeisElXCDAxilBEsYA-o_YqQYMGRRqgHpgFEzshc/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/bH1rPmIdLHM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Support Team

[Presentation slides](https://docs.google.com/presentation/d/1fccSNBOFCUCatckO4zBevnApHNsk2OTO8QPSgeIGI_4/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/rBP4RXaX1L0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!

----
