---
layout: job_family_page
title: "Corporate Development"
---

## Coporate Development Analysis

### Analyst, Corporate Development 

As an Analyst, Corporate Development, you will be responsible for identifying,
sourcing, and owning the acquisition opportunity pipeline, and helping with the
deal flow.

#### Job Grade

The Analyst, Corporate Development is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

* Create a target list of potential acquisitions
* Develop relationships with tech incubators, investors, and other sources of acquisition candidates
* Execute a sourcing plan through online research, outreach, and other means to help support the corporate development team goals
* Operationalize acquisition sourcing and handling processes to help the team scale
* Validate fit for terms, product roadmap, and other criteria
* Value the companies in a financial model
* Help negotiate terms of the deal
* Manage deal flow CRM system data
* Prepare all acquisition related documents such as Non-Disclosure Agreements, Letters of Intent, and Purchase Agreements with our legal team and legal counsel
* Assist in various aspects of the deal as necessary: qualification, initial rationale/pitch, financial modeling, due diligence and closing & integration activities.

#### Requirements

* Bachelors or equivalent in Finance, Accounting, Economics, Computer Science, Engineering, or a related field
* Minimum of 2 years of corporate development, venture capital, private equity, or competitive analysis ideally focused on the technology industry
* Adept with technology and a strategic thinker – knows what’s best for the business
* Excellent judgment, mature personality, and experience working with executives; a sophisticated, worldly businessperson
* Proactive and action-oriented, anticipates needs
* Experience in a deal environment and buyer-seller conducts
* Excellent verbal and written communication skills
* Familiarity with the DevOps space
* You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
* You share our [values](/handbook/values/), and work in accordance with those values
* Ability to use GitLab

### Associate, Corporate Development

As an Associate, Corporate Development, you will be responsible for identifying,
sourcing, and owning the acquisition opportunity pipeline, and helping with the
deal flow.

#### Job Grade

The Associate, Corporate Development is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

* Create a target list of potential acquisitions
* Develop relationships with tech incubators, investors, and other sources of acquisition candidates
* Execute a sourcing plan through online research, outreach, and other means to help support the corporate development team goals
* Operationalize acquisition sourcing and handling processes to help the team scale
* Validate fit for terms, product roadmap, and other criteria
* Value the companies in a financial model
* Help negotiate terms of the deal
* Manage deal flow CRM system data
* Prepare all acquisition related documents such as Non-Disclosure Agreements, Letters of Intent, and Purchase Agreements with our legal team and legal counsel
* Assist in various aspects of the deal as necessary: qualification, initial rationale/pitch, financial modeling, due diligence and closing & integration activities.

#### Requirements

* Bachelors or equivalent in Finance, Accounting, Economics, Computer Science, Engineering, or a related field
* Minimum of 5 years of corporate development, venture capital, private equity, or competitive analysis ideally focused on the technology industry
* Minimum of 2 years in acquisitions
* Adept with technology and a strategic thinker – knows what’s best for the business
* Excellent judgment, mature personality, and experience working with executives; a sophisticated, worldly businessperson
* Proactive and action-oriented, anticipates needs
* Experience in a deal environment and buyer-seller conducts
* Excellent verbal and written communication skills
* Familiarity with the DevOps space
* You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
* You share our [values](/handbook/values/), and work in accordance with those values

## Coporate Development

### Director, Corporate Development

As a Director, Corporate Development, you will be responsible for sourcing,
negotiating, and closing [acquisitions](/handbook/acquisitions/).

#### Job Grade

The Director, Corporate Development is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

* Own end-to-end deal management
  * Create a target list of potential acquisitions
  * Validate fit for terms, product roadmap, and other criteria
  * Value the companies in a financial model
  * Negotiate terms of the deal
  * Prepare all acquisition related documents such as Non-Disclosure Agreements, Letters of Intent, and Purchase Agreements with our legal team and legal counsel
  * Integrate the acquisition into the company
* Provide project management leadership over the pre-LOI validation and due diligence stages of the acquisition process with cross-functional teams, including communication of diligence findings
* Establish collaborative, effective, and trusting relationships with key internal functions including Product, Engineering, Legal, Finance, and Marketing to ensure the execution of an efficient acquisition process
* Ensure a proper level of strategic, operational, and organizational alignment.

#### Requirements

* Over 5 years of relevant acquisition experience
* Relationship builder with the ability to establish a dialog with leadership of acquisition targets.
* Experience structuring various types of deal terms
* Skilled in corporate valuation, risk management, financial modeling, negotiation, and integration
* Ability to manage multiple priorities and projects cross-functionally with strong organizational skills
* Exemplary verbal and written communication and presentation skills.
* Demonstrated analytical and data led decision-making
* Self-starter and team player with ability to achieve or exceed their objectives while working in concert with others
* You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
* You share our [values](/handbook/values/), and work in accordance with those values

#### Performance Indicators

* [Acquisition velocity](/handbook/product/performance-indicators/)
* [Acquisition success](/handbook/product/performance-indicators/)
* [Qualified acquisition targets](/handbook/product/performance-indicators/)

### Senior Director of Corporate Development

As the Senior Director of Corporate Development, you will be responsible for
building the team to source, negotiate, and close [acquisitions](/handbook/acquisitions/).

#### Job Grade

The Senior Director of Corporate Development is a [grade 11](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

* Grow and manage the corporate development team
* Own end-to-end deal management
  * Create a target list of potential acquisitions
  * Validate fit for terms, product roadmap, and other criteria
  * Value the companies in a financial model
  * Negotiate terms of the deal
  * Prepare all acquisition related documents such as Non-Disclosure Agreements, Letters of Intent, and Purchase Agreements with our legal team and legal counsel
  * Integrate the acquisition into the company
* Provide project management leadership over the pre-LOI validation and due diligence stages of the acquisition process with cross-functional teams, including communication of diligence findings
* Establish collaborative, effective, and trusting relationships with key internal functions including Product, Engineering, Legal, Finance, and Marketing to ensure the execution of an efficient acquisition process
* Ensure a proper level of strategic, operational, and organizational alignment.

#### Requirements

* Experience growing and managing an acquisitions team
* 10 years of relevant work experience in acquisitions
* Relationship builder with the ability to establish a dialogue with leadership team members of potential acquisition targets.
* Experience structuring various types of deal terms
* Strong negotiation abilities
* Ability to manage multiple priorities and projects cross-functionally with strong organizational skills
* Exemplary verbal and written communication and presentation skills.
* Demonstrated analytical and data led decision-making
* Self-starter and team player with ability to achieve or exceed their objectives while working in concert with others
* You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
* You share our [values](/handbook/values/), and work in accordance with those values

#### Performance Indicators

* [Acquisition velocity](/handbook/product/performance-indicators/)
* [Acquisition success](/handbook/product/performance-indicators/)
* [Qualified acquisition targets](/handbook/product/performance-indicators/)

## Career Ladder

The next step in the Corporate Development job family is to move to a VP role in Corporate Development which is not yet defined at GitLab. 
